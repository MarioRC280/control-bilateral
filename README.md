# Control bilateral de un robot teleoperado - Matlab #

Dado un sistema de telemanipulación maestro/esclavo.

### What is this repository for? ###

Desarrollar un esquema de reflexión de fuerzas en Matlab/Simuling con reflexión de fuerzas, usando:

* Transmisión de variables de energía (velocidad y fuerza).
* Transmisión de variables de onda.
* [Consultar los datos en `Guión control bilateral.pdf`]
